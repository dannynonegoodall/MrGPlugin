package org.bletchcraft.mrgplugin;

import co.aikar.commands.BukkitCommandIssuer;
import co.aikar.commands.PaperCommandManager;
import com.onarandombox.MultiverseCore.MultiverseCore;
import com.onarandombox.MultiverseCore.api.MVWorldManager;
import org.bletchcraft.mrgplugin.commands.MrGCommand;
import org.bletchcraft.mrgplugin.helpers.BletchLogger;
import org.bletchcraft.mrgplugin.helpers.ConfigManager;
import org.bletchcraft.mrgplugin.helpers.MrGUtils;
import org.bletchcraft.mrgplugin.helpers.WorldManager;
import org.bletchcraft.mrgplugin.listeners.*;
import org.bukkit.World;
import org.bukkit.WorldType;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

public final class MrGPlugin extends JavaPlugin {
    private static MrGPlugin plugin;
    private static PaperCommandManager commandManager;
    public final ConfigManager configManager = new ConfigManager(this);
    public final WorldManager worldManager = new WorldManager(this);
    public BletchLogger bletchLogger;
    private MultiverseCore multiverseCore = null;
    private MVWorldManager mvWorldManager = null;
    public MrGUtils mrGUtils = new MrGUtils(this);
    public Object bletchcraftServer = null;

    @Override
    public void onEnable() {
        plugin = this;
        bletchLogger = new BletchLogger(this);
        multiverseCore = getMultiverseCore();
        mvWorldManager = getMVWorldManager();
        // Plugin startup logic
        getConfig().options().copyDefaults(true);
        saveDefaultConfig();
        // Let's see if we can find the bletchcraftserver plugin. Because we don't have a jar dependency on it, we can't
        // use the normal method of getting a reference to it. We'll have to use reflection to get a reference to it.
        bletchcraftServer = getBletchcraftServerReference();

        registerCommands();
        registerListeners();
    }

    public Object getBletchcraftServerReference() {
        bletchcraftServer = (Object) plugin.getServer().getPluginManager().getPlugin("BletchCraftServer");
        return bletchcraftServer;
    }

    public MultiverseCore getMultiverseCore() {
        if (multiverseCore== null) {
            multiverseCore = (MultiverseCore) getServer().getPluginManager().getPlugin("Multiverse-Core");
        }
        return multiverseCore;
    }

    public MVWorldManager getMVWorldManager() {
        if (mvWorldManager== null) {
            mvWorldManager = getMultiverseCore().getMVWorldManager();
        }
        return mvWorldManager;
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    private void registerCommands() {
        commandManager = new PaperCommandManager(this);
        commandManager.enableUnstableAPI("help");
        commandManager.registerCommand(new MrGCommand(this));

        commandManager.getCommandCompletions().registerAsyncCompletion("worldwarps", (c) -> {
            BukkitCommandIssuer issuer = (BukkitCommandIssuer) c.getIssuer();
            CommandSender sender = c.getSender();
            if (sender instanceof Player player) {
                World world = player.getWorld();
                return configManager.warpsInWorld(world);
            } else {
                return List.of();
            }
        });

        commandManager.getCommandCompletions().registerAsyncCompletion("allknownworlds", (c) -> {
            BukkitCommandIssuer issuer = (BukkitCommandIssuer) c.getIssuer();
            CommandSender sender = c.getSender();
            if (sender instanceof Player player) {
                return mrGUtils.getAllWorldNamesNew(true);
            } else {
                return List.of();
            }
        });

        commandManager.getCommandCompletions().registerAsyncCompletion("allloadedworlds", (c) -> {
            BukkitCommandIssuer issuer = (BukkitCommandIssuer) c.getIssuer();
            CommandSender sender = c.getSender();
            if (sender instanceof Player player) {
                return mrGUtils.getAllWorldNamesNew(false);
            } else {
                return List.of();
            }
        });

        commandManager.getCommandCompletions().registerAsyncCompletion("worldtypes", (c) -> {
            BukkitCommandIssuer issuer = (BukkitCommandIssuer) c.getIssuer();
            CommandSender sender = c.getSender();
            if (sender instanceof Player player) {
                return mrGUtils.getAllWorldTypes();
            } else {
                return List.of();
            }
        });

        // register completions for the different values of TreeType
        commandManager.getCommandCompletions().registerAsyncCompletion("treetypes", (c) -> {
            BukkitCommandIssuer issuer = (BukkitCommandIssuer) c.getIssuer();
            CommandSender sender = c.getSender();
            if (sender instanceof Player player) {
                return mrGUtils.getAllTreeTypes(true);
            } else {
                return List.of();
            }
        });
    }

    private void registerListeners() {
        getServer().getPluginManager().registerEvents(new BlockBurnListener(this), this);
        getServer().getPluginManager().registerEvents(new EntitySpawnListener(this), this);
        getServer().getPluginManager().registerEvents(new BlockExplodeListener(this), this);
        getServer().getPluginManager().registerEvents(new EntityExplodeListener(this), this);
        getServer().getPluginManager().registerEvents(new BlockBreakListener(this), this);
        getServer().getPluginManager().registerEvents(new PlayerJoinListener(this), this);
        getServer().getPluginManager().registerEvents(new ChangedWorldListener(this), this);
        getServer().getPluginManager().registerEvents(new CommandBlockEventListener(this), this);
        getServer().getPluginManager().registerEvents(new BlockPistonExtendEventListener(this), this);
        getServer().getPluginManager().registerEvents(new BlockPistonRetractEventListener(this), this);
    }

    // Typical Bukkit Plugin Scaffolding
    public static MrGPlugin getPlugin() {
        return plugin;
    }

    // A way to access your command manager from other files if you do not use a Dependency Injection approach
    public static PaperCommandManager getCommandManager() {
        return commandManager;
    }
}
