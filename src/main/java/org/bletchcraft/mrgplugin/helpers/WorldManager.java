package org.bletchcraft.mrgplugin.helpers;

import com.onarandombox.MultiverseCore.MVWorld;
import com.onarandombox.MultiverseCore.MultiverseCore;
import com.onarandombox.MultiverseCore.api.MVWorldManager;
import com.onarandombox.MultiverseCore.api.MultiverseWorld;
import com.onarandombox.MultiverseCore.commands.TeleportCommand;
import org.bletchcraft.mrgplugin.MrGPlugin;
import org.bukkit.*;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class WorldManager {
    private final MrGPlugin plugin;
    public MrGUtils mrGUtils;

    // https://github.com/Multiverse/Multiverse-Core/wiki/Developer-API-Starter#getting-the-multiverse-plugin-instances
    public WorldManager(final MrGPlugin thePlugin) {
        plugin = thePlugin;
        mrGUtils = new MrGUtils(thePlugin);

        // readConfiguration();
    }

    public List<String> getAllWorldNamesOld() {
        List<String> worldNames = new ArrayList<>();
        for (World world : plugin.getServer().getWorlds()) {
            worldNames.add(world.getName());
        }
        for (String worldName : plugin.getMVWorldManager().getUnloadedWorlds()) {
            if (!worldNames.contains(worldName)) {
                worldNames.add(worldName);
            }
        }
        return worldNames;
    }


    public List<MultiverseWorld> getAllWorlds() {
        // Add the loaded worlds to the worlds list.
        List<MultiverseWorld> worlds = new ArrayList<>();
        worlds.addAll(plugin.getMVWorldManager().getMVWorlds());

        return worlds;
    }

    public String changeWorld(String newWorldName, Player player) {
        // Using the MultiverseCore API - https://ci.onarandombox.com/job/Multiverse-Core/javadoc/com/onarandombox/MultiverseCore/api/MVWorldManager.html

        String oldWorldName = getCurrentWorldName(player);
        StringBuilder result = new StringBuilder();
        boolean changeWorldResult, validWorld;
        boolean loadWorldResult, unloadWorldResult;
        validWorld = plugin.getMVWorldManager().isMVWorld(newWorldName);
        loadWorldResult = plugin.getMVWorldManager().loadWorld(newWorldName);
        if (loadWorldResult) {
            // Now create a teleport command and teleport to the new world
            TeleportCommand teleportCommand = new TeleportCommand(plugin.getMultiverseCore());

            // Initialise a list of strings to hold the arguments for the teleport command and add the streing newWorldName as the first element
            List<String> teleportArgs = new ArrayList<>();
            teleportArgs.add(newWorldName);
            teleportCommand.runCommand(player, teleportArgs);

            unloadWorldResult = plugin.getMVWorldManager().unloadWorld(oldWorldName);
            if (!unloadWorldResult) {
                unloadWorldResult = plugin.getMVWorldManager().unloadWorld(oldWorldName.toLowerCase());
            }
            if (!unloadWorldResult) {
                result.append("The current world: ")
                        .append(oldWorldName)
                        .append(" could not be unloaded.");
            }
        } else {
            result.append("The world you are trying to load: ")
                    .append(newWorldName)
                    .append(" could not be loaded.");
        }
        return result.toString();
    }

    public String removeWorld(String worldToRemove, Player player){
        // Using the MultiverseCore API - https://ci.onarandombox.com/job/Multiverse-Core/javadoc/com/onarandombox/MultiverseCore/api/MVWorldManager.html

        StringBuilder result = new StringBuilder();
        boolean removeWorldResult;
        String currentWorldName;

        removeWorldResult = plugin.getMVWorldManager().deleteWorld(worldToRemove);
        if (removeWorldResult) {
            result.append("The world: ")
                    .append(mrGUtils.wrapGreenWhite(worldToRemove))
                    .append(" was removed.");
        } else {
            result.append(ChatColor.RED)
                    .append("The world: ")
                    .append(worldToRemove)
                    .append(" could not be removed.")
                    .append(ChatColor.WHITE);
        }
        return result.toString();
    }

    public String createWorld(String newWorldName, Player player, String worldTypeString, boolean generateStructures, GameMode gameMode, String seedString) {
        // Using the MultiverseCore API - https://ci.onarandombox.com/job/Multiverse-Core/javadoc/com/onarandombox/MultiverseCore/api/MVWorldManager.html

        StringBuilder result = new StringBuilder();
        StringBuilder message = new StringBuilder();
        boolean createWorldResult, validWorld;
        World.Environment env;
        WorldType worldType;

        // Assume that the validation has already been carried out - that this world does not already exist
        env = player.getWorld().getEnvironment();
        worldType = WorldType.valueOf(worldTypeString);

        // Build the message to send to the player
        message.append(ChatColor.WHITE)
                .append("Starting the creation of world: ")
                .append(mrGUtils.wrapGreenWhite(newWorldName))
                .append(" with the following parameters: Environment: ")
                .append(mrGUtils.wrapGreenWhite(env.toString()))
                .append(", WorldType: ")
                .append(mrGUtils.wrapGreenWhite(worldType.toString()))
                .append(", GenerateStructures: ")
                .append(mrGUtils.wrapGreenWhite(Boolean.toString(generateStructures)))
                .append(", GameMode: ")
                .append(mrGUtils.wrapGreenWhite(gameMode.toString()))
                .append(", Seed: ");
        if (seedString == null) {
            message.append(mrGUtils.wrapGreenWhite("(not specified)"));
        } else {
            message.append(mrGUtils.wrapGreenWhite(seedString));
        }
        message.append(".")
                .append(ChatColor.WHITE);

        player.sendMessage(message.toString());

        // Create the world in Multiverse
        createWorldResult = plugin
                .getMVWorldManager()
                .addWorld(newWorldName, env, seedString, worldType, generateStructures, "NORMAL");

        // Check if the world was created and if so, set the game mode and set the world to not autoload
        if (createWorldResult) {
            result.append(ChatColor.WHITE)
                    .append("The world: ")
                    .append(mrGUtils.wrapGreenWhite(newWorldName))
                    .append(" was created.");
            MultiverseWorld createdWorld = plugin.getMVWorldManager().getMVWorld(newWorldName);
            if (createdWorld.setGameMode(gameMode)) {
                result.append(" With game mode set to ")
                        .append(mrGUtils.wrapGreenWhite(gameMode.toString()))
                        .append(".");
            } else {
                result.append(ChatColor.RED)
                        .append(" Could not set game mode to ")
                        .append(gameMode.toString())
                        .append(".")
                        .append(ChatColor.WHITE);
            }

            createdWorld.setAutoLoad(false);
            result.append(" Multiverse will not autoload it.");
        } else {
            // If the world was not created, then send a message to the player
            result.append(ChatColor.RED)
                    .append("The world: ")
                    .append(newWorldName)
                    .append(" could not be created.")
                    .append(ChatColor.WHITE);
        }
        return result.toString();
    }

    public String getCurrentWorldName(Player player) {
        return player.getWorld().getName();
    }


}
