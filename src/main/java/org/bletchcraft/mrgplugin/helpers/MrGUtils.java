package org.bletchcraft.mrgplugin.helpers;

import com.onarandombox.MultiverseCore.MultiverseCore;
import com.onarandombox.MultiverseCore.api.MVWorldManager;
import com.onarandombox.MultiverseCore.api.MultiverseWorld;
import net.kyori.adventure.audience.Audience;
import net.kyori.adventure.bossbar.BossBar;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.NamedTextColor;
import net.kyori.adventure.title.Title;
import org.bletchcraft.mrgplugin.MrGPlugin;
import org.bukkit.*;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.Vector;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class MrGUtils {
    final public MrGPlugin plugin;

    public MrGUtils(final MrGPlugin thePlugin) {
        plugin = thePlugin;
    }

    public Location parseXYZ(World world, String x, String y, String z) {
        final Location location;
        final Integer xInt, yInt, zInt;
        xInt = tryParseIntOrNull(x);
        yInt = tryParseIntOrNull(y);
        zInt = tryParseIntOrNull(z);

        if (xInt == null || yInt == null || zInt == null) {
            return null;
        }
        location = new Location(world, xInt.doubleValue(), yInt.doubleValue(), zInt.doubleValue());
        return location;
    }


    public List<Vector> spiralVectors(int maxVectors, double distance) {
        List<Vector> result = new ArrayList<>();
        double angle = 0.0;
        for (int i = 0; i < maxVectors; i++) {
            double x = distance * i * Math.cos(angle);
            double z = distance * i * Math.sin(angle);
            result.add(new Vector(x, 0d, z));
            angle += Math.PI / 2;  // Each quarter turn
            if (i % 4 == 0) {     // Increase distance every full revolution
                distance += distance;
            }
        }
        return result;
    }

    public List<Vector> spiralVectors(int x, int y, int distance) {
        List<Vector> result = new ArrayList<>();
        int x1 = x;
        int y1 = y;
        int dx = 0;
        int dy = -distance;
        int t = Math.max(x * distance, y * distance);
        int maxI = t * t;
        // Log all local varaibles
        plugin.bletchLogger.logInfo("x: " + x);
        plugin.bletchLogger.logInfo("y: " + y);
        plugin.bletchLogger.logInfo("distance: " + distance);
        plugin.bletchLogger.logInfo("x1: " + x1);
        plugin.bletchLogger.logInfo("y1: " + y1);
        plugin.bletchLogger.logInfo("dx: " + dx);
        plugin.bletchLogger.logInfo("dy: " + dy);
        plugin.bletchLogger.logInfo("t: " + t);
        plugin.bletchLogger.logInfo("maxI: " + maxI);

        // Log "starting loop"
        plugin.bletchLogger.logInfo("starting loop");
        for (int i = 0; i < maxI; i += distance) {
            plugin.bletchLogger.logInfo("  i: " + i);
            if ((-x / 2 <= x1) && (x1 <= x / 2) && (-y / 2 <= y1) && (y1 <= y / 2)) {
                Vector v = new Vector(x1, 0, y1);
                // Log i and v
                plugin.bletchLogger.logInfo("  v: " + v);
                result.add(v);
            }
            if ((x1 == y1) || ((x1 < 0) && (x1 == -y1)) || ((x1 > 0) && (x1 == 1 - y1))) {
                t = dx;
                dx = -dy;
                dy = t;
            }
            x1 += dx;
            y1 += dy;
        }
        return result;
    }

    public List<Vector> rectangleVectors(int count, int distance) {
        List<Vector> result = new ArrayList<>();
        int oneSide = (int) Math.sqrt((double) count);
        int otherSide = count / oneSide;

        for (int xStep = 0; xStep < oneSide; xStep++) {
            for (int zStep = 0; zStep < otherSide; zStep++) {
                result.add(new Vector(xStep * distance, 0, zStep * distance));
            }
        }
        return result;
    }

    public List<String> getServerWorldNames() {
        List<String> worldNames = new ArrayList<String>();
        for (World world : plugin.getServer().getWorlds()) {
            worldNames.add(world.getName());
        }
        return worldNames;
    }

    public List<String> getEntityTypes() {
        List<String> entityTypes = new ArrayList<>();
        for (EntityType entityType : EntityType.values()) {
            entityTypes.add(entityType.toString());
        }
        return entityTypes;
    }

    public String wrapGreenWhite(String text) {
        return ChatColor.GREEN + text + ChatColor.WHITE;
    }

    public String wrapWhiteGreen(String text) {
        return ChatColor.WHITE + text + ChatColor.GREEN;
    }

    public String wrapRedWhite(String text) {
        return ChatColor.RED + text + ChatColor.WHITE;
    }

    public String wrapWorld(String worldString) {
        return wrapGreenWhite(worldString);
    }

    public String wrapWarp(String warpName) {
        return wrapGreenWhite(warpName);
    }

    public static String wrapEntityType(String entityType) {
        return ChatColor.GREEN + entityType + ChatColor.WHITE;
    }

    public String wrapBlock(String blockName) {
        return wrapGreenWhite(blockName);
    }

    public String wrapError(String errorText) {
        return wrapRedWhite(errorText);
    }

    public void sayValidWorlds(Player player) {
        String validWorlds = String.join(", ", getServerWorldNames());
        player.sendMessage("Valid worlds for this server are: " + wrapWorld(validWorlds));
    }

    private static boolean checkIfIsWorld(File worldFolder) {
        if (worldFolder.isDirectory()) {
            File[] files = worldFolder.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File file, String name) {
                    return name.toLowerCase().endsWith(".dat");
                }
            });
            if (files != null && files.length > 0) {
                return true;
            }
        }
        return false;
    }


    public List<String> getPotentialWorlds() {
        // Potential worlds holds all files found
        List<String> potentialWorlds = new ArrayList<>();
        // world names holds just proven worlds
        List<String> worldNames = new ArrayList<>();

        File worldFolder = this.plugin.getServer().getWorldContainer();
        if (worldFolder == null) {
            return worldNames;
        }
        // Get all files in the world folder
        File[] files = worldFolder.listFiles();

        Collection<MultiverseWorld> worlds = plugin.getMVWorldManager().getMVWorlds();
        for (MultiverseWorld world : worlds) {
            potentialWorlds.add(world.getName());
        }
        for (String world : plugin.getMVWorldManager().getUnloadedWorlds()) {
            potentialWorlds.add(world);
        }
        for (File file : files) {
            if (file.isDirectory() && checkIfIsWorld(file) && !potentialWorlds.contains(file.getName())) {
                worldNames.add(file.getName());
            }
        }
        return worldNames;
    }

    public List<String> getAllWorldTypes() {
        List<String> worldTypeNames = new ArrayList<>();
        for (WorldType worldType : WorldType.values()) {
            worldTypeNames.add(worldType.toString());
        }
        return worldTypeNames;
    }

    public List<String> getAllTreeTypes(boolean addRandom) {
        List<String> treeTypeNames = new ArrayList<>();
        for (TreeType treeType : TreeType.values()) {
            treeTypeNames.add(treeType.toString());
        }
        if (addRandom) {
            treeTypeNames.add("RANDOM");
        }
        return treeTypeNames;
    }

    public String getRandomTreeType(boolean addRandom) {
        List<String> treeTypeNames = getAllTreeTypes(addRandom);
        int randomIndex = (int) (Math.random() * treeTypeNames.size());
        return treeTypeNames.get(randomIndex);
    }

    public List<String> getAllWorldNamesNew(boolean includeUnloadedWorlds) {
        List<String> allWorlds = new ArrayList<>();

        Collection<MultiverseWorld> worlds = plugin.getMVWorldManager().getMVWorlds();
        for (MultiverseWorld world : worlds) {
            allWorlds.add(world.getName());
        }
        // If we only want the loaded worlds then return now
        if (!includeUnloadedWorlds) {
            return allWorlds;
        }

        // Otherwise, let's get the unloaded worlds
        for (String world : plugin.getMVWorldManager().getUnloadedWorlds()) {
            allWorlds.add(world);
        }

        // Now go through the folders and files that are present and see if any of them looks like a world
        File worldFolder = this.plugin.getServer().getWorldContainer();
        if (worldFolder == null) {
            return allWorlds;
        }

        // Get all files in the world folder
        File[] files = worldFolder.listFiles();
        for (File file : files) {
            // If we haven't already seen it and it is a directory and it looks like a world then add it
            if ((!allWorlds.contains(file.getName()) && file.isDirectory() && checkIfIsWorld(file))) {
                allWorlds.add(file.getName());
            }
        }

        return allWorlds;
    }

    public void sayValidMulitverseWorlds(Player player, boolean includeUnloadedWorlds) {
        String validWorlds = String.join(", ", getAllWorldNamesNew(includeUnloadedWorlds));
        player.sendMessage("Valid worlds for this server are: " + wrapWorld(validWorlds));
    }

    public String getWorldNameFromArgs(String[] args, Player player) {
        // Return either the world name or an empty string to signify an incorrect number of params
        String worldName;
        if (args.length == 0) {
            // No args so get the players current world
            worldName = player.getWorld().getName();
        } else if (args.length == 1) {
            worldName = args[0];
        } else {
            // Will this re-display the usage?
            worldName = "";
        }
        return worldName;
    }

    public String getEntityTypeFromArgs(String[] args, Player player) {
        // Return either the world name or an empty string to signify an incorrect number of params
        String entityType = "";
        if (args.length == 1) {
            // Must be a way to see if the type exists in the Enum!?!
            plugin.bletchLogger.logWarning("Attempting to find EntityType: " + args[0]);
            if (Arrays.stream(EntityType.values()).anyMatch(e -> e.toString().equalsIgnoreCase(args[0]))) {
                entityType = args[0];
            } else {
                entityType = "";
            }
        }
        return entityType;
    }

    public int tryParseIntOrDefault(String value, int defaultVal) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return defaultVal;
        }
    }

    public Integer tryParseIntOrNull(String value) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public Location parseXYZOrNull(World world, String x, String y, String z) {
        final Location location;
        final Integer xInt, yInt, zInt;
        xInt = tryParseIntOrNull(x);
        yInt = tryParseIntOrNull(y);
        zInt = tryParseIntOrNull(z);

        if (xInt == null || yInt == null || zInt == null) {
            return null;
        }
        location = new Location(world, xInt.doubleValue(), yInt.doubleValue(), zInt.doubleValue());
        return location;
    }

    public void showTitle(Player player, String title, String subTitle) {
        if (title == null) {
            title = "";
        }
        if (subTitle == null) {
            subTitle = "";
        }
        if (title.isEmpty() && subTitle.isEmpty()) {
            return;
        }
        final Component componentTitle = Component.text(title, NamedTextColor.WHITE);
        final Component componentSubtitle = Component.text(subTitle, NamedTextColor.GRAY);

        // Creates a simple title with the default values for fade-in, stay on screen and fade-out durations
        final Title theTitle = Title.title(componentTitle, componentSubtitle);

        // Send the title to your audience
        ((Audience) player).showTitle(theTitle);
    }

    public ArmorStand createHologram(Location location, String text, NamedTextColor namedColor) {
        ArmorStand hologram;
        final Component componentText;
        PersistentDataContainer metaData;

        hologram = (ArmorStand) location.getWorld().spawnEntity(location, EntityType.ARMOR_STAND);
        hologram.setGravity(false);
        hologram.setVisible(false);
        hologram.setAI(false);
        hologram.setInvulnerable(true);
        hologram.setCustomNameVisible(true);
        componentText = Component.text(text, NamedTextColor.WHITE);
        hologram.customName(componentText);

        return hologram;
    }


    public ArmorStand getHologram(Location location) {
        // Let's look for an Armorstand near this location
        World world = location.getWorld();
        ArrayList<Entity> entities = (ArrayList<Entity>) world.getNearbyEntities(location, 1, 1, 1);
        if (entities.isEmpty() || entities.get(0).getType() != EntityType.ARMOR_STAND) {
            return null;
        } else {
            return (ArmorStand) entities.get(0);
        }
    }

    public Plugin getBletchCraftPlugin() {
        return Bukkit.getPluginManager().getPlugin("BletchCraftServer");
    }
}