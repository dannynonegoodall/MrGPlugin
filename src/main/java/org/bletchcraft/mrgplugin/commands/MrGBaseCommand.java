package org.bletchcraft.mrgplugin.commands;

import co.aikar.commands.BaseCommand;
import org.bletchcraft.mrgplugin.MrGPlugin;
import org.bletchcraft.mrgplugin.helpers.MrGUtils;

public class MrGBaseCommand extends BaseCommand {
    final public MrGPlugin plugin;
    public MrGUtils mrGUtils;

    public MrGBaseCommand(final MrGPlugin thePlugin) {
        plugin = thePlugin;
        mrGUtils = plugin.mrGUtils;
    }

}
