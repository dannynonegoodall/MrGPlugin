package org.bletchcraft.mrgplugin.commands;

import co.aikar.commands.CommandHelp;
import co.aikar.commands.annotation.*;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.IncompleteRegionException;
import com.sk89q.worldedit.LocalSession;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.extension.platform.Actor;
import com.sk89q.worldedit.extent.inventory.BlockBag;
import net.kyori.adventure.text.format.NamedTextColor;
import org.bletchcraft.mrgplugin.MrGPlugin;
import org.bletchcraft.mrgplugin.helpers.MrGUtils;
import org.bletchcraft.mrgplugin.helpers.WorldManager;
import org.bukkit.*;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.util.Vector;
import static org.joor.Reflect.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@CommandAlias("mrg")
public class MrGCommand extends MrGBaseCommand {

    public MrGCommand(MrGPlugin thePlugin) {
        super(thePlugin);
    }

    @Subcommand("DumpObject|DO")
    @Description("Given and ID, dumps the object from the BletchCraft server to the log file.")
    @Syntax("<Number>")
    public void dumpObject(CommandSender sender, int number) {
        plugin.bletchLogger.logInfo("DumpObject Command called:");
        plugin.bletchLogger.logInfo("  Number: " + number);
        plugin.bletchLogger.logInfo("  Sending command to BletchCraftServer");
        // Create an array of String that can be passed to the api method
        String[] args = new String[1];
        args[0] = Integer.toString(number);
        boolean result = on(plugin.bletchcraftServer).call("api", "dumpObject", args).get();
        if (result) {
            sender.sendMessage("The object was dumped to the log file.");
            plugin.bletchLogger.logInfo("  The object was dumped to the log file.");
        } else {
            sender.sendMessage("The object was NOT dumped to the log file.");
            plugin.bletchLogger.logWarning("  The object was NOT dumped to the log file.");
        }
    }


    @Subcommand("ShowObject|SO")
    @Description("Given an ID, shows the object from the BletchCraft server in the console.")
    @Syntax("<Number>")
    public void showObject(CommandSender sender, int number) {
        plugin.bletchLogger.logInfo("ShowObject Command called:");
        plugin.bletchLogger.logInfo("  Number: " + number);
        plugin.bletchLogger.logInfo("  Sending command to BletchCraftServer");
        // Create an array of String that can be passed to the api method
        String[] args = new String[1];
        args[0] = Integer.toString(number);
        String[] result = on(plugin.bletchcraftServer).call("api", "getobjectdump", args).get();
        if (result != null && result.length > 0) {
            // Loop through the array of strings and send each one to the player
            sender.sendMessage("Here is the object dump for object " + number + ":");
            for (String line : result) {
                sender.sendMessage(line);
            }
            plugin.bletchLogger.logInfo("  The object was shown in the console.");
        } else {
            sender.sendMessage("Could not show object ID: " + number + " in the console.");
            plugin.bletchLogger.logWarning("  The object was NOT shown in the console.");
        }
    }

    @Subcommand("Protect|Pro")
    @Description("Protects blocks in the world from being broken, exploded or burned.")
    @Syntax("<WorldName>")
    @CommandCompletion("@worlds")
    public void protect(Player player, World world) {
        final String worldName = world.getName();
        final boolean success;

        plugin.bletchLogger.logWarning(String.format("protect got player of: %s and a world of: %s", player.getName(), world.getName()));
        if (mrGUtils.getServerWorldNames().stream().anyMatch(e -> e.equals(worldName))) {
            success = plugin.configManager.protectWorld(worldName, player);
            if (success) {
                player.sendMessage("The world " + ChatColor.GREEN + worldName + ChatColor.WHITE + " was protected.");
            }
        } else {
            player.sendMessage(ChatColor.RED + "The world '" + worldName + "' was not found on the server.");
            mrGUtils.sayValidWorlds(player);
            plugin.bletchLogger.logWarning(String.format("World name: '%s' was not found on the server.", worldName));
        }
    }

    @Subcommand("ProtectStatus|ProStat")
    @Description("Shows if a world is protected or not.")
    public void protectStatus(Player player, World world) {
        StringBuilder output = new StringBuilder();
        String protectedWorldList;
        String excludedWorldList;

        plugin.reloadConfig();
        protectedWorldList = plugin.configManager.protectedWorldList();
        excludedWorldList = plugin.configManager.excludedWorldList();

        output.append("The following worlds are protected: ");
        output.append(ChatColor.GREEN);
        output.append(protectedWorldList.isEmpty() ? "(No worlds are protected)" : protectedWorldList);
        output.append(ChatColor.WHITE);
        output.append(". The following worlds are excluded: ");
        output.append(ChatColor.GREEN);
        output.append(excludedWorldList.isEmpty() ? "(No worlds are excluded)" : excludedWorldList);
        output.append(ChatColor.WHITE);
        output.append(".");

        player.sendMessage(output.toString());
        mrGUtils.sayValidWorlds(player);
        plugin.bletchLogger.logInfo("ProtectStatusCommand called:");
    }

    @Subcommand("RemoveWorld|RW")
    @CommandCompletion("@allknownworlds @nothing")
    @Syntax("<WorldName> <CONFIRM>")
    @Description("Removes a world from the server and deletes all files. This is UNRECOVERABLE. Use with caution.")
    public void removeWorld(Player player, String worldName, @Optional String confirmString) {
        String result;
        boolean confirm;
        WorldManager worldManager = new WorldManager(plugin);

        // set confirm based on the value of confirmString. Assume a default of false unless the confirm string is "CONFIRM"
        if (confirmString != null && confirmString.equals("CONFIRM")) {
            confirm = true;
        } else {
            confirm = false;
        }

        // Check that the worldName is not the same is the current world name using getCurrentWorldName(). If it is the send a red message to the player and return
        if (worldName.equalsIgnoreCase(worldManager.getCurrentWorldName(player))) {
            player.sendMessage(ChatColor.RED + "You cannot remove the world you are currently in.");
            return;
        }

        if (confirm) {
            result = plugin.worldManager.removeWorld(worldName, player);
            if (result.isEmpty()) {
                result = new StringBuilder()
                        .append("The world ")
                        .append(mrGUtils.wrapGreenWhite(worldName))
                        .append(" was removed.")
                        .toString();
            }
        } else {
            result = mrGUtils.wrapRedWhite("You must confirm the removal of the world by typing 'CONFIRM' after the world name.");
        }
        player.sendMessage(result);
    }

    @Subcommand("CreateWorld|CRW")
    @CommandCompletion("@worldtypes @nothing CREATIVE|SPECTATOR|SURVIVAL|ADVENTURE TRUE|FALSE @nothing")
    @Syntax("<WorldType> <WorldName> <GameMode> <GenerateStructures> [Seed]")
    @Description("Creates a new Multiverse world.")
    public void createWorld(Player player, String worldType, String worldName, String gameModeString, String generateStructuresString, @Optional String seedString) {
        String result;
        boolean generateStructures;

        // Set some default values if they weren't specified
        generateStructures = (!generateStructuresString.equalsIgnoreCase("FALSE"));
        if (gameModeString.isEmpty()) {
            gameModeString = "CREATIVE";
        }
        GameMode gameMode = GameMode.valueOf(gameModeString);
        if (worldType.isEmpty()) {
            worldType = "NORMAL";
        }
        if (seedString == null) {
            seedString = "";
        }

        // Validate that the worldName does not already exist
        if (mrGUtils.getAllWorldNamesNew(true).stream().anyMatch(e -> e.equals(worldName))) {
            player.sendMessage(ChatColor.RED + "The world '" + worldName + "' already exists on the server.");
            mrGUtils.sayValidWorlds(player);
            plugin.bletchLogger.logWarning(String.format("World name: '%s' already exists on the server.", worldName));
        } else {
            // Create the world, set the gamemode and set it to not autoload
            result = plugin.worldManager.createWorld(worldName, player, worldType, generateStructures, gameMode, seedString);
            if (result.isEmpty()) {
                player.sendMessage(new StringBuilder()
                        .append("The world ")
                        .append(mrGUtils.wrapGreenWhite(worldName))
                        .append(" was created.")
                        .toString());
            } else {
                player.sendMessage(result);
            }
        }
    }

    @Subcommand("ChangeWorld|CHW")
    @CommandCompletion("@allknownworlds")
    @Syntax("<WorldName>")
    @Description("Changes the world and unloads the previous world.")
    public void changeWorld(Player player, String worldName) {
        WorldManager worldManager = new WorldManager(plugin);
        String result;

        if (mrGUtils.getAllWorldNamesNew(true).stream().anyMatch(e -> e.equals(worldName))) {
            result = plugin.worldManager.changeWorld(worldName, player);
            if (result.isEmpty()) {
                player.sendMessage(new StringBuilder()
                        .append("The world was changed to ")
                        .append(ChatColor.GREEN + worldName + ChatColor.WHITE)
                        .append(".").toString());
            } else {
                player.sendMessage(ChatColor.RED + result);
            }
        } else {
            player.sendMessage(ChatColor.RED + "The world '" + worldName + "' was not found on the server.");
            mrGUtils.sayValidWorlds(player);
            plugin.bletchLogger.logWarning(String.format("World name: '%s' was not found on the server.", worldName));
        }
    }

    @Subcommand("AlwaysDay|AD")
    @CommandCompletion("TRUE|FALSE")
    @Syntax("[TRUE|FALSE]")
    @Description("Sets the world to always be day or to allow the usual daylight/night cycle.")
    public void alwaysDay(Player player, @Optional String alwaysDayString) {
        // Log the values of player and alwaysDayString
        plugin.bletchLogger.logWarning(String.format("alwaysDay got player of: %s and a alwaysDayString of: %s", player.getName(), alwaysDayString));
        World world = player.getWorld();
        boolean alwaysDay;
        StringBuilder message = new StringBuilder();
        String trueText = "allow the usual day and night times";
        String falseText = "always be day";

        // If we've had no arugments send back the status
        if (alwaysDayString == null || alwaysDayString.isEmpty()) {
            //Send a message to the player with the current setting of the GameRule.DoDaylightCycle
            message.append("The daylight cycle in the world: ")
                    .append(mrGUtils.wrapGreenWhite(world.getName()))
                    .append(" is currently set to ");
            if (world.getGameRuleValue(GameRule.DO_DAYLIGHT_CYCLE)) {
                message.append(mrGUtils.wrapGreenWhite(trueText));
            } else {
                message.append(mrGUtils.wrapGreenWhite(falseText));
            }
            message.append(".");
            player.sendMessage(message.toString());
            return;
        }

        message.append("Setting the world: ")
                .append(mrGUtils.wrapGreenWhite(world.getName()))
                .append(" to ");
        if (alwaysDayString.equalsIgnoreCase("TRUE")) {
            alwaysDay = true;
            message.append(mrGUtils.wrapGreenWhite(falseText))
                    .append(".");
        } else if (alwaysDayString.equalsIgnoreCase("FALSE")) {
            alwaysDay = false;
            message.append(mrGUtils.wrapGreenWhite(trueText))
                    .append(".");
        } else {
            player.sendMessage(mrGUtils.wrapRedWhite("You must specify either TRUE or FALSE."));
            return;
        }
        if (alwaysDay) {
            world.setTime(1000);
        }
        if (world.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, !alwaysDay)) {
            player.sendMessage(message.toString());
        } else {
            player.sendMessage(mrGUtils.wrapRedWhite("Something went wrong setting the game rule."));
        }
        ;
    }

    @Subcommand("AlwaysClear|AC")
    @CommandCompletion("TRUE|FALSE")
    @Syntax("[TRUE|FALSE]")
    @Description("Sets the world to always be clear or to allow the usual weather cycle.")
    public void alwaysClear(Player player, @Optional String alwaysClearString) {
        // Log the values of player and alwaysClearString
        plugin.bletchLogger.logWarning(String.format("alwaysClear got player of: %s and a alwaysClearString of: %s", player.getName(), alwaysClearString));
        World world = player.getWorld();
        boolean alwaysClear;
        StringBuilder message = new StringBuilder();
        String trueText = "allow the usual clear, rain and thunder";
        String falseText = "always be clear";

        // If we've had no arugments send back the status
        if (alwaysClearString == null || alwaysClearString.isEmpty()) {
            //Send a message to the player with the current setting of the GameRule.DoWeatherCycle
            message.append("The weather cycle in the world: ")
                    .append(mrGUtils.wrapGreenWhite(world.getName()))
                    .append(" is currently set to ");
            if (world.getGameRuleValue(GameRule.DO_WEATHER_CYCLE)) {
                message.append(mrGUtils.wrapGreenWhite(trueText));
            } else {
                message.append(mrGUtils.wrapGreenWhite(falseText));
            }
            message.append(".");
            player.sendMessage(message.toString());
            return;
        }

        message
                .append("Setting the weather in world: ")
                .append(mrGUtils.wrapGreenWhite(world.getName()))
                .append(" to ");
        if (alwaysClearString.equalsIgnoreCase("TRUE")) {
            alwaysClear = true;
            message.append(mrGUtils.wrapGreenWhite(falseText));
        } else if (alwaysClearString.equalsIgnoreCase("FALSE")) {
            alwaysClear = false;
            message.append(mrGUtils.wrapGreenWhite(trueText));
        } else {
            player.sendMessage(ChatColor.RED + "You must specify either TRUE or FALSE.");
            return;
        }
        if (world.setGameRule(GameRule.DO_WEATHER_CYCLE, !alwaysClear)) {
            player.sendMessage(message.toString());
        } else {
            player.sendMessage(mrGUtils.wrapRedWhite("Something went wrong setting the game rule."));
        }
        ;
    }

    @Subcommand("day")
    @Description("Sets the time to day.")
    public void day(Player player) {
        World world = player.getWorld();
        StringBuilder message = new StringBuilder();
        world.setTime(1000);
        message.append("The time in the world: ")
                .append(mrGUtils.wrapGreenWhite(world.getName()))
                .append(" was set to")
                .append(mrGUtils.wrapGreenWhite(" day"))
                .append(".");
        player.sendMessage(message.toString());
    }

    @Subcommand("night")
    @Description("Sets the time to night.")
    public void night(Player player) {
        World world = player.getWorld();
        StringBuilder message = new StringBuilder();
        world.setTime(13000);
        message.append("The time in the world: ")
                .append(mrGUtils.wrapGreenWhite(world.getName()))
                .append(" was set to")
                .append(mrGUtils.wrapGreenWhite(" night"))
                .append(".");
        player.sendMessage(message.toString());
    }

    // Write a Kill sub command that lets the player kill all types of the specified mob/entity. The method will be
    // passed the EntityType and the player. It will then iterate through all of the entities in the world and kill
    // all of the ones that match the EntityType. It will return the number of entities that were killed.

    @Subcommand("Kill|K")
    @CommandCompletion("@mobs")
    @Syntax("<MobName>")
    @Description("Kills all of the specified mob type in the world.")
    public void kill(Player player, World world, EntityType entityType) {
        String entityTypeName = entityType.name();
        int killCount = 0;

        if (entityTypeName.isEmpty()) {
            player.sendMessage("The entity type is either not recognised or was blank.");
        }
        // If we're here then we have a valid entityType
        List<Entity> entities = world.getEntities();
        for (Entity entity : entities) {
            if (entity.getType() == entityType) {
                entity.remove();
                killCount++;
            }
        }
        if (killCount > 0) {
            player.sendMessage(new StringBuilder()
                    .append("Killed ")
                    .append(mrGUtils.wrapGreenWhite(Integer.toString(killCount)))
                    .append(" entities of type: ")
                    .append(mrGUtils.wrapGreenWhite(entityTypeName))
                    .append(".").toString()
            );
        } else {
            player.sendMessage(new StringBuilder()
                    .append("No entities of type: ")
                    .append(mrGUtils.wrapGreenWhite(entityTypeName))
                    .append(" were found.")
                    .toString()
            );
        }
    }

    // Write a command that takes a mob type and then loops through all entities of that type in the world
    // and teleports them to the location of the player plus or minus a random number of blocks in the X and Z
    // less than 5
    @Subcommand("Gather|G")
    @CommandCompletion("@mobs")
    @Syntax("<MobName>")
    @Description("Gathers all of the specified mob type in the world to the player.")
    public void gather(Player player, World world, EntityType entityType) {
        String entityTypeName = entityType.name();
        int gatherCount = 0;

        if (entityTypeName.isEmpty()) {
            player.sendMessage("The entity type is either not recognised or was blank.");
            return;
        }
        // If we're here then we have a valid entityType
        List<Entity> entities = world.getEntities();
        for (Entity entity : entities) {
            int randomX = new Random().nextInt(10) - 5;
            int randomZ = new Random().nextInt(10) - 5;
            if (entity.getType() == entityType) {
                entity.teleport(player.getLocation().add(randomX, 0, randomZ));
                gatherCount++;
            }
        }
        if (gatherCount > 0) {
            player.sendMessage(new StringBuilder()
                    .append("Gathered ")
                    .append(mrGUtils.wrapGreenWhite(Integer.toString(gatherCount)))
                    .append(" entities of type: ")
                    .append(mrGUtils.wrapGreenWhite(entityTypeName))
                    .append(" to your location.")
                    .toString()
            );
        } else {
            player.sendMessage(new StringBuilder()
                    .append("No entities of type: ")
                    .append(mrGUtils.wrapGreenWhite(entityTypeName))
                    .append(" were found.")
                    .toString()
            );
        }
    }

    // Write a command that changes the players walk speed. The command will take a number between 0 and 10
    // and set the player's walk speed to that number. The number will be divided by 10 to get the actual walk speed
    // so 10 will be 1.0, 5 will be 0.5 and 1 will be 0.1. The command will return the player's current walk speed
    // and the new walk speed.
    // If the walk speed is not specified then the command will send a message to the player with their current walk
    // speed and the valid range of values.
    @Subcommand("WalkSpeed|WS")
    @CommandCompletion("DEFAULT|1|2|3|4|5|6|7|8|9|10")
    @Syntax("[WalkSpeed]")
    @Description("Sets the player's walk speed.")
    public void walkSpeed(Player player, @Optional String walkSpeedString) {
        StringBuilder message = new StringBuilder();
        float walkSpeed;
        final float defaultWalkSpeed = 0.2f;
        float currentWalkSpeed = player.getWalkSpeed();

        // If we've had no arugments send back the status
        if (walkSpeedString == null || walkSpeedString.isEmpty()) {
            //Send a message to the player with the current setting of walkspeed
            message.append("Your walk speed is currently set to ")
                    .append(mrGUtils.wrapGreenWhite(String.format("%.0f", currentWalkSpeed * 10)))
                    .append(" which is ")
                    .append(mrGUtils.wrapGreenWhite(String.format("%.0f", currentWalkSpeed * 100)))
                    .append("% of the maximum speed and ")
                    .append(mrGUtils.wrapGreenWhite(String.format("%.0f", currentWalkSpeed / defaultWalkSpeed * 100)))
                    .append("% of the normal speed.");
            player.sendMessage(message.toString());
            return;
        }

        // If we're here then we have a valid walkSpeedString
        try {
            if (walkSpeedString.equalsIgnoreCase("DEFAULT")) {
                walkSpeed = defaultWalkSpeed * 10;
            } else {
                walkSpeed = Float.parseFloat(walkSpeedString);
            }
        } catch (NumberFormatException e) {
            player.sendMessage(mrGUtils.wrapRedWhite("You must specify a number between 1 and 10."));
            return;
        }
        if (walkSpeed < 0 || walkSpeed > 10) {
            player.sendMessage(mrGUtils.wrapRedWhite("You must specify a number between 1 and 10."));
            return;
        }
        player.setWalkSpeed(walkSpeed / 10);
        message.append("Your walk speed was changed from ")
                .append(mrGUtils.wrapGreenWhite(String.format("%.0f", currentWalkSpeed)))
                .append(" to ")
                .append(mrGUtils.wrapGreenWhite(String.format("%.0f", walkSpeed)))
                .append(" which is ")
                .append(mrGUtils.wrapGreenWhite(String.format("%.0f", walkSpeed * 10)))
                .append("% of the maximum speed and ")
                .append(mrGUtils.wrapGreenWhite(String.format("%.0f", walkSpeed / 10 / defaultWalkSpeed * 100)))
                .append("% of the normal speed.");

        player.sendMessage(message.toString());
    }

    // Write a subcommand that adjusts the players flyspeed in the same way that the walkspeed method does
    @Subcommand("FlySpeed|FS")
    @CommandCompletion("DEFAULT|1|2|3|4|5|6|7|8|9|10")
    @Syntax("[FlySpeed]")
    @Description("Sets the player's flying speed. The default value is 1.")
    public void flySpeed(Player player, @Optional String flySpeedString) {
        StringBuilder message = new StringBuilder();
        float flySpeed;
        final float defaultFlySpeed = 0.1f;
        float currentFlySpeed = player.getFlySpeed();

        // If we've had no arugments send back the status
        if (flySpeedString == null || flySpeedString.isEmpty()) {
            //Send a message to the player with the current setting of flyspeed
            message.append("Your fly speed is currently set to ")
                    .append(mrGUtils.wrapGreenWhite(String.format("%.0f", currentFlySpeed * 10)))
                    .append(" which is ")
                    .append(mrGUtils.wrapGreenWhite(String.format("%.0f", currentFlySpeed * 100)))
                    .append("% of the maximum speed and ")
                    .append(mrGUtils.wrapGreenWhite(String.format("%.0f", currentFlySpeed / defaultFlySpeed * 100)))
                    .append("% of the normal speed.");
            player.sendMessage(message.toString());
            return;
        }

        // If we're here then we have a valid flySpeedString
        try {
            if (flySpeedString.equalsIgnoreCase("DEFAULT")) {
                flySpeed = defaultFlySpeed * 10;
            } else {
                flySpeed = Float.parseFloat(flySpeedString);
            }
        } catch (NumberFormatException e) {
            player.sendMessage(mrGUtils.wrapRedWhite("You must specify a number between 1 and 10."));
            return;
        }
        if (flySpeed < 0 || flySpeed > 10) {
            player.sendMessage(mrGUtils.wrapRedWhite("You must specify a number between 1 and 10."));
            return;
        }
        player.setFlySpeed(flySpeed / 10);
        message.append("Your fly speed was changed from ")
                .append(mrGUtils.wrapGreenWhite(String.format("%.0f", currentFlySpeed)))
                .append(" to ")
                .append(mrGUtils.wrapGreenWhite(String.format("%.0f", flySpeed)))
                .append(" which is ")
                .append(mrGUtils.wrapGreenWhite(String.format("%.0f", flySpeed * 10)))
                .append("% of the maximum speed and ")
                .append(mrGUtils.wrapGreenWhite(String.format("%.0f", flySpeed / 10 / defaultFlySpeed * 100)))
                .append("% of the normal speed.");

        player.sendMessage(message.toString());
    }

    // Write a subcommand that takes no arguments called ShowWinScreen. When run it will call the showWinScreen method
    // on the Player object.
    @Subcommand("ShowWinScreen")
    @Description("Shows the player the win screen.")
    public void showWinScreen(Player player) {
        player.showWinScreen();
    }

    // Write a command called explosion. It will accept two arguments - the first will be whether the explosion will
    // create a fire. The second will be the power of the explosion. The command will then create an explosion at the
    // position the player is looking at.
    @Subcommand("Explosion|Ex")
    @CommandCompletion("TRUE|FALSE 1|2|3|4|5|6|7|8|9|10|20|30|40|50|100")
    @Syntax("[TRUE|FALSE] [Power]")
    @Description("Creates an explosion at the position the player is looking at.")
    public void explosion(Player player, @Optional String fireString, @Optional String powerString) {
        StringBuilder message = new StringBuilder();
        boolean fire;
        float power;
        Location location = player.getTargetBlock(null, 100).getLocation();

        // If we've had no arugments send back the status
        if (fireString == null || fireString.isEmpty()) {
            fire = false;
        } else {
            fire = fireString.equalsIgnoreCase("TRUE");
        }
        if (powerString == null || powerString.isEmpty()) {
            power = 4;
        } else {
            try {
                power = Float.parseFloat(powerString);
            } catch (NumberFormatException e) {
                player.sendMessage(mrGUtils.wrapRedWhite("You must specify a number between 1 and 100."));
                return;
            }
            if (power < 0 || power > 100) {
                player.sendMessage(mrGUtils.wrapRedWhite("You must specify a number between 1 and 100."));
                return;
            }
        }
        player.getWorld().createExplosion(location, power, fire);
        message.append("An explosion was created at ")
                .append(mrGUtils.wrapGreenWhite(Integer.toString(location.getBlockX())))
                .append(", ")
                .append(mrGUtils.wrapGreenWhite(Integer.toString(location.getBlockY())))
                .append(", ")
                .append(mrGUtils.wrapGreenWhite(Integer.toString(location.getBlockZ())))
                .append(" with a power of ")
                .append(mrGUtils.wrapGreenWhite(Float.toString(power)));
        if (fire) {
            message.append(" that will create a fire.");
        } else {
            message.append(" that will NOT create a fire.");
        }
        player.sendMessage(message.toString());
    }

    //Write a command called PlantTree that will create a tree at the location the player is looking at. The command
    // will ask the player to select the TreeType from a list of available trees. The command will also ask the player
    // to specify whether the tree should be a large tree or not. The command will then create the tree at the location
    // the player is looking at.
    @Subcommand("PlantTree|PT")
    @CommandCompletion("@treetypes")
    @Syntax("<TreeType>")
    @Description("Creates a tree at the position the player is looking at.")
    public void plantTree(Player player, @Optional String treeTypeString) {
        StringBuilder message = new StringBuilder();
        TreeType treeType;
        Location location = player.getTargetBlock(null, 100).getLocation();
        location = location.add(0, 1, 0);

        // If we've had no arugments send back the status
        if (treeTypeString == null || treeTypeString.isEmpty()) {
            player.sendMessage(mrGUtils.wrapRedWhite("You must specify a tree type."));
            return;
        } else {
            if (treeTypeString.equalsIgnoreCase("RANDOM")) {
                treeTypeString = mrGUtils.getRandomTreeType(false);
            }
            try {
                treeType = TreeType.valueOf(treeTypeString);
            } catch (IllegalArgumentException e) {
                player.sendMessage(mrGUtils.wrapRedWhite("You must specify a valid tree type."));
                return;
            }
        }
        player.getWorld().generateTree(location, treeType);
        message.append("A ")
                .append(mrGUtils.wrapGreenWhite(treeType.toString()))
                .append(" tree was created at ")
                .append(mrGUtils.wrapGreenWhite(Integer.toString(location.getBlockX())))
                .append(", ")
                .append(mrGUtils.wrapGreenWhite(Integer.toString(location.getBlockY())))
                .append(", ")
                .append(mrGUtils.wrapGreenWhite(Integer.toString(location.getBlockZ())))
                .append(".");
        player.sendMessage(message.toString());
    }

    // Write a subcommand that generates a specified number of trees of a specified type and a specified distance
    // apart. The command will take three arguments - the first will be the type of tree, the second will be the
    // number of trees to generate and the third will be the distance apart. The command will then generate the
    // specified number of trees of the specified type at the specified distance apart in a radiating pattern starting
    // at the block the player is looking at. If the number of trees is not specified a number of 10 will be assumed.
    // If the distance apart is not specified then a distance of 5 will be assumed.

    @Subcommand("GenerateTrees|GT")
    @CommandCompletion("@treetypes 9|10|20|30|40|50|100 1|2|3|4|5|6|7|8|9|10|20|30|40|50|100")
    @Syntax("<TreeType> [NumberOfTrees] [DistanceApart]")
    @Description("Generates a number of trees of a specified type and a specified distance apart.")
    public void generateTrees(Player player, @Optional String treeTypeString, @Optional String numberOfTreesString, @Optional String distanceApartString) {
        StringBuilder message = new StringBuilder();
        TreeType treeType;
        int numberOfTrees;
        int distanceApart;
        Location location = player.getTargetBlock(null, 100).getLocation();
        location = location.add(0, 1, 0);

        // If we've had no arugments send back the status
        if (treeTypeString == null || treeTypeString.isEmpty()) {
            player.sendMessage(mrGUtils.wrapRedWhite("You must specify a tree type."));
            return;
        } else {
            try {
                if (treeTypeString.equalsIgnoreCase("RANDOM")) {
                    treeType = TreeType.valueOf(mrGUtils.getRandomTreeType(false));
                } else {
                    treeType = TreeType.valueOf(treeTypeString);
                }
            } catch (IllegalArgumentException e) {
                player.sendMessage(mrGUtils.wrapRedWhite("You must specify a valid tree type."));
                return;
            }
        }
        if (numberOfTreesString == null || numberOfTreesString.isEmpty()) {
            numberOfTrees = 10;
        } else {
            try {
                numberOfTrees = Integer.parseInt(numberOfTreesString);
            } catch (NumberFormatException e) {
                player.sendMessage(mrGUtils.wrapRedWhite("You must specify a number between 9 and 100."));
                return;
            }
            if (numberOfTrees < 9 || numberOfTrees > 100) {
                player.sendMessage(mrGUtils.wrapRedWhite("You must specify a number between 9 and 100."));
                return;
            }
        }
        if (distanceApartString == null || distanceApartString.isEmpty()) {
            distanceApart = 5;
        } else {
            try {
                distanceApart = Integer.parseInt(distanceApartString);
            } catch (NumberFormatException e) {
                player.sendMessage(mrGUtils.wrapRedWhite("You must specify a number between 9 and 100."));
                return;
            }
            if (distanceApart < 0 || distanceApart > 100) {
                player.sendMessage(mrGUtils.wrapRedWhite("You must specify a number between 9 and 100."));
                return;
            }
        }
        // loop through the number of trees to be generated and calculate the next location in a spiral pattern that starts at location and then spirals out
        // distanceApart blocks away from the previous location. Generate a tree at that location.
        for (Vector v : mrGUtils.rectangleVectors(numberOfTrees, distanceApart)) {
            Location nextLocation = location.clone();
            if (treeTypeString.equalsIgnoreCase("RANDOM")) {
                try {
                    treeType = TreeType.valueOf(mrGUtils.getRandomTreeType(false));
                    // Log the treeType that was selected at random
                    plugin.bletchLogger.logWarning(String.format("generateTrees got a random tree type of: %s", treeType.toString()));
                } catch (IllegalArgumentException e) {
                    // Log an error. We got an invalid tree
                    plugin.bletchLogger.logError(String.format("generateTrees got an invalid tree type of: %s", treeTypeString));
                    return;
                }
            }
            player.getWorld().generateTree(nextLocation.add(v), treeType);


            // Generate a message that summarises the number of trees generated
            message.append("Generated ")
                    .append(mrGUtils.wrapGreenWhite(Integer.toString(numberOfTrees)))
                    .append(" ")
                    .append(mrGUtils.wrapGreenWhite(treeTypeString))
                    .append(" trees ")
                    .append(mrGUtils.wrapGreenWhite(Double.toString(distanceApart)))
                    .append(" blocks apart starting at ")
                    .append(mrGUtils.wrapGreenWhite(Integer.toString(location.getBlockX())))
                    .append(", ")
                    .append(mrGUtils.wrapGreenWhite(Integer.toString(location.getBlockY())))
                    .append(", ")
                    .append(mrGUtils.wrapGreenWhite(Integer.toString(location.getBlockZ())))
                    .append(".");
            player.sendMessage(message.toString());
        }
    }

    // Write a subcommand that calls the setAllowFlight method on the player. The command should be
    // called AllowFlight and it should accept a single argument of TRUE|FALSE
    @Subcommand("AllowFlight|AF")
    @CommandCompletion("TRUE|FALSE")
    @Syntax("[TRUE|FALSE]")
    @Description("Sets the player's allow flight status.")
    public void allowFlight(Player player, @Optional String allowFlightString) {
        StringBuilder message = new StringBuilder();
        boolean allowFlight;

        // If we've had no arugments send back the status
        if (allowFlightString == null || allowFlightString.isEmpty()) {
            //Send a message to the player with the current setting of allowFlight
            message.append("Your allow flight status is currently set to ")
                    .append(mrGUtils.wrapGreenWhite(Boolean.toString(player.getAllowFlight())))
                    .append(".");
            player.sendMessage(message.toString());
            return;
        }

        // If we're here then we have a valid allowFlightString
        if (allowFlightString.equalsIgnoreCase("TRUE")) {
            allowFlight = true;
        } else if (allowFlightString.equalsIgnoreCase("FALSE")) {
            allowFlight = false;
        } else {
            player.sendMessage(mrGUtils.wrapRedWhite("You must specify either TRUE or FALSE."));
            return;
        }
        player.setAllowFlight(allowFlight);
        message.append("Your allow flight status was changed to ")
                .append(mrGUtils.wrapGreenWhite(Boolean.toString(allowFlight)))
                .append(".");
        player.sendMessage(message.toString());
    }


    @Subcommand("Exclude|Exc")
    @CommandCompletion("@worlds")
    @Syntax("<WorldName>")
    @Description("Excluded worlds cannot be protected.")
    public void exclude(Player player, World world) {
        String worldName = world.getName();
        boolean success;

        success = plugin.configManager.excludeWorld(worldName, player);
        if (success) {
            player.sendMessage("The world '" + mrGUtils.wrapWorld(worldName) + "' was marked as excluded.");
        }
    }

    @Subcommand("UnProtect|UnP")
    @CommandCompletion("@worlds")
    @Syntax("<WorldName>")
    @Description("If a world has been protected, this command will unprotected it.")
    public void unProtect(Player player, World world) {
        final String worldName = world.getName();
        final boolean success;

        success = plugin.configManager.unprotectWorld(worldName, player);
        if (success) {
            player.sendMessage("The world " + ChatColor.GREEN + worldName + ChatColor.WHITE + " was unprotected.");
        }
    }

    @Subcommand("UnExclude|UnE")
    @CommandCompletion("@worlds")
    @Syntax("<WorldName>")
    @Description("If a world has been excluded, this command will unexcluded it and it can be protected again.")
    public void unExclude(Player player, World world) {
        final String worldName = world.getName();
        final boolean success;

        success = plugin.configManager.unexcludeWorld(worldName, player);
        if (success) {
            player.sendMessage("The world " + ChatColor.GREEN + worldName + ChatColor.WHITE + " was unexcluded - removed from the excluded list.");
        }
    }

    @Subcommand("SpawnStatus|SS")
    @Description("Shows a list of mobs that are not allowed to spawn in any world.")
    public void spawnStatus(Player player, World world) {
        final String doNotSpawnList;
        StringBuilder output = new StringBuilder();

        plugin.reloadConfig();
        doNotSpawnList = String.join(", ", plugin.getConfig().getStringList("DoNotSpawn"));
        output.append("The following entities are stopped from spawning: ");
        output.append(mrGUtils.wrapEntityType(doNotSpawnList));
        output.append(".");
        player.sendMessage(output.toString());
    }

    @Subcommand("DoSpawn|DS")
    @CommandCompletion("@mobs")
    @Syntax("<MobName>")
    @Description("Allows this mob type to spawn in any world.")
    public void doSpawn(Player player, World world, EntityType entityType) {
        String entityTypeName = entityType.name();

        if (entityTypeName.isEmpty()) {
            player.sendMessage("The entity type is either not recognised or was blank.");
        }
        // If we're here then we have a valid entityType
        if (plugin.configManager.doSpawn(entityTypeName)) {
            player.sendMessage(
                    String.format(
                            "Spawning of entity type: '%s' has now been enabled.",
                            MrGUtils.wrapEntityType(entityTypeName)
                    )
            );
        } else {
            player.sendMessage(
                    ChatColor.RED + String.format(
                            "Entities of type: '%s' could already spawn because they were not stopped from spawning.",
                            entityType
                    )
            );
        }
    }

    @Subcommand("DoNotSpawn|DNS")
    @CommandCompletion("@mobs")
    @Syntax("<MobName>")
    @Description("Stops this mob type from spawning in any world.")
    public void doNotSpawn(Player player, World world, EntityType entityType) {
        String entityTypeName = entityType.name();

        if (entityTypeName.isEmpty()) {
            player.sendMessage("The entity type is either not recognised or was blank.");
        }
        // If we're here then we have a valid entityType
        if (plugin.configManager.doNotSpawn(entityTypeName)) {
            player.sendMessage(
                    String.format(
                            "Spawning of entity type: '%s' has been stopped.",
                            mrGUtils.wrapEntityType(entityTypeName)
                    )
            );
        } else {
            player.sendMessage(
                    ChatColor.RED + String.format(
                            "Entities of type: '%s' were already stopped from spawning.",
                            entityType
                    )
            );
        }
    }

    @Subcommand("DelWarp|DW")
    @Description("Deletes and forgets about a previously saved warp.")
    @Syntax("<WarpName>")
    @CommandCompletion("@worldwarps @boolean")
    public void delWarp(CommandSender sender, String warpName) {
        if (sender instanceof Player player) {
            World world;
            Location warpLocation;
            ArmorStand hologram;

            if (warpName == null || warpName.isEmpty()) {
                player.sendMessage(mrGUtils.wrapError("That warp name could not be found."));
                return;
            }
            world = player.getWorld();
            if (!plugin.configManager.warpExists(world, warpName)) {
                player.sendMessage(
                        String.format(
                                "The warp: %s does not exist in the world: %s so it can't be deleted.",
                                mrGUtils.wrapWarp(warpName),
                                mrGUtils.wrapWorld(world.getName()
                                )
                        )
                );
                return;
            }
            warpLocation = plugin.configManager.getWarpVector(warpName, world);
            plugin.configManager.removeWarp(warpName, world);

            // Now look for our hologram
            hologram = mrGUtils.getHologram(warpLocation);
            if (hologram == null) {
                plugin.bletchLogger.logWarning(
                        String.format(
                                "When deleting warp %s the hologram could not be found.",
                                warpName
                        )
                );
                return;
            }
            // Let's make sure that this armorstand has the PersistentData
            PersistentDataContainer metaData = hologram.getPersistentDataContainer();
            if (metaData.has(new NamespacedKey(plugin, "warp"), PersistentDataType.STRING)) {
                // This is our Armorstand / hologram so we can delete it
                hologram.remove();
            }
            player.sendMessage(
                    String.format(
                            "The warp: %s has been deleted from the world: %s",
                            mrGUtils.wrapWarp(warpName),
                            mrGUtils.wrapWorld(world.getName())
                    )
            );
        }
    }

    @Subcommand("SetWarp|SW")
    @Description("Sets a warp to the current location or the location specified.")
    @Syntax("<WarpName> [X] [Y] [Z]")
    @CommandCompletion("@nothing @nothing @nothing @nothing")
    public void setWarp(CommandSender sender, String warpName, @Default("null") String
            warpLocationX, @Default("null") String warpLocationY, @Default("null") String warpLocationZ) {
        Location location = null;
        String errorMessage = "";
        Boolean result;

        if (sender instanceof Player player) {
            World world = player.getWorld();
            ArmorStand hologram;
            PersistentDataContainer metaData;

            plugin.bletchLogger.logWarning("player " + player.getName());
            plugin.bletchLogger.logWarning("world " + world.getName());
            plugin.bletchLogger.logWarning("warpName " + warpName);
            plugin.bletchLogger.logWarning("warpLocationX " + warpLocationX);
            plugin.bletchLogger.logWarning("warpLocationY " + warpLocationY);
            plugin.bletchLogger.logWarning("warpLocationZ " + warpLocationZ);

            // Validate that if any of the warpLocation parameters are set then

            if (!(warpLocationX + warpLocationY + warpLocationZ).equalsIgnoreCase("nullnullnull")) {
                // Some parameters were set so the location we get back cannot be null
                location = mrGUtils.parseXYZOrNull(world, warpLocationX, warpLocationY, warpLocationZ);
                // If it is null then we couldn't decode the location
                if (location == null) {
                    player.sendMessage(
                            String.format(
                                    "I couldn't understand the location" + mrGUtils.wrapWorld(" (%s, %s, %s)"),
                                    warpLocationX,
                                    warpLocationY,
                                    warpLocationZ
                            )
                    );
                    return;
                }
            }
            // If we got here and the location is null then it's because it wasn't specified. We assume it's the player's
            // current position
            if (location == null) {
                location = player.getLocation();
            }

            //Now validate that the warpName doesn't already exist and that it is shorter than 32 characters
            if (warpName.length() > 32) {
                errorMessage = "The warp name '" + mrGUtils.wrapWarp(warpName) + "' is too long. It must be 32 characters or fewer.";
            } else if (plugin.configManager.warpExists(world, warpName)) {
                errorMessage = "The warp named '" + mrGUtils.wrapWarp(warpName) + "' already exists.";
            }
            if (!errorMessage.isEmpty()) {
                player.sendMessage("Couldn't add warp. " + errorMessage);
                return;
            }
            // If we got here then we should have a valid warpName, location and world
            result = plugin.configManager.addWarp(warpName, location);
            if (!result) {
                player.sendMessage(mrGUtils.wrapError("Something when wrong trying to add this warp!"));
            } else {
                // Create a hologram at the warp point
                hologram = mrGUtils.createHologram(location, "WARP: " + warpName, NamedTextColor.WHITE);
                metaData = (PersistentDataContainer) hologram.getPersistentDataContainer();
                metaData.set(new NamespacedKey(plugin, "warp"), PersistentDataType.STRING, warpName);

            }
        }
    }

    @Subcommand("ShowWarps|SW")
    @Description("Shows a list of all of the saved warps in a world.")
    @CommandCompletion("@worlds")
    @Syntax("[WorldName]")
    public void showWarps(CommandSender sender, @Optional World world) {
        if (sender instanceof Player player) {
            List<String> warpsInWorld = new ArrayList<String>();
            String message;
            if (world == null) {
                world = player.getWorld();
            }
            warpsInWorld = plugin.configManager.warpsInWorld(world);
            if (warpsInWorld.isEmpty()) {
                message = String.format(
                        "There are no saved warps in the world: %s.",
                        mrGUtils.wrapWorld(world.getName())
                );
            } else {
                message = String.format(
                        "In world %s the follow warps exist:: %s",
                        mrGUtils.wrapWorld(world.getName()),
                        mrGUtils.wrapWarp(String.join(", ", warpsInWorld))
                );
            }
            player.sendMessage(message);
        }
    }

    @Subcommand("WarpTo|WT")
    @Description("Teleports a player to the location of the saved warp.")
    @CommandCompletion("@worldwarps")
    @Syntax("<WarpName>")
    public void warpTo(CommandSender sender, String warpName) {
        if (sender instanceof Player player) {
            World world = player.getWorld();
            Location location;
            if (warpName == null || warpName.isEmpty() || !plugin.configManager.warpExists(world, warpName)) {
                player.sendMessage(
                        String.format(
                                "You need to specify a warp that has been saved in this world (%s).",
                                mrGUtils.wrapWorld(world.getName())
                        )
                );
                return;
            }
            // We have a valid warpName
            location = plugin.configManager.getWarpVector(warpName, world);
            if (location == null) {
                player.sendMessage(mrGUtils.wrapError("Could not find that saved warp."));
                return;
            }
            player.teleport(location);
            mrGUtils.showTitle(
                    player,
                    warpName,
                    String.format(
                            "@ (%d, %d, %d)",
                            location.getBlockX(),
                            location.getBlockY(),
                            location.getBlockZ()
                    )
            );
        }
    }

    @Subcommand("Hologram|Holo")
    @Description("Creates a hologram where the player is standing")
    @Syntax("<HologramText>")
    public void doHologram(CommandSender sender, String[] hologramText) {
        if (sender instanceof Player player) {
            Location playerLocation;
            StringBuilder sb = new StringBuilder();
            ArmorStand hologram;

            if (hologramText == null || hologramText.length == 0) {
                player.sendMessage("I didn't understand the hologram command. Make sure you set the hologram text.");
                return;
            }
            // Build the string
            for (String hologramWord : hologramText) {
                if (!sb.isEmpty()) {
                    sb.append(" ");
                }
                sb.append(hologramWord);
            }
            playerLocation = player.getLocation();
            hologram = mrGUtils.createHologram(playerLocation, sb.toString(), NamedTextColor.WHITE);
        }
    }

    // Create a subcommand called hologramat that takes a location and a string and creates a hologram at that location
    // with the string as the text. The location will be specified as X Y Z and the string will be the text to display
    // in the hologram. The command will return the location of the hologram.
    @Subcommand("HologramAt|HoloAt")
    @Description("Creates a hologram at the specified location")
    @Syntax("<X> <Y> <Z> <HologramText>")
    public void doHologramAt(CommandSender sender, String hologramLocationX, String hologramLocationY, String hologramLocationZ, String[] hologramText) {
        if (sender instanceof Player player) {
            Location location;
            StringBuilder sb = new StringBuilder();
            ArmorStand hologram;

            if (hologramLocationX == null || hologramLocationY == null || hologramLocationZ == null) {
                player.sendMessage("I didn't understand the HologramAt command. Make sure you set the hologram location.");
                return;
            }
            if (hologramText == null || hologramText.length == 0) {
                player.sendMessage("I didn't understand the HologramAt command. Make sure you set the hologram text.");
                return;
            }
            // Build the string
            for (String hologramWord : hologramText) {
                if (!sb.isEmpty()) {
                    sb.append(" ");
                }
                sb.append(hologramWord);
            }
            // Get the location
            location = mrGUtils.parseXYZ(player.getWorld(), hologramLocationX, hologramLocationY, hologramLocationZ);
            if (location == null) {
                player.sendMessage("I didn't understand the HologramAt command. Make sure you set the hologram location.");
                return;
            }
            hologram = mrGUtils.createHologram(location, sb.toString(), NamedTextColor.WHITE);
        }
    }

    // Create a subcommand called undo that attempts to understand how many worldEdit sessions can be undone
    // and then undoes that many sessions.


    @Subcommand("UndoAll|UA")
    @Syntax("[NumberOfUndoSessions]")
    @Description("Attempts to undo world edit sessions.")
    public void doUndo(CommandSender sender, @Optional String numberOfUndoSessionsString) {
        int numberOfUndoSessions;
        StringBuilder message = new StringBuilder();

        if (numberOfUndoSessionsString == null || numberOfUndoSessionsString.isEmpty()) {
            numberOfUndoSessions = 1000;
        } else {
            try {
                numberOfUndoSessions = Integer.parseInt(numberOfUndoSessionsString);
            } catch (NumberFormatException e) {
                sender.sendMessage(mrGUtils.wrapRedWhite("You must specify a number between 1 and 1000."));
                return;
            }
            if (numberOfUndoSessions < 1 || numberOfUndoSessions > 1000) {
                sender.sendMessage(mrGUtils.wrapRedWhite("You must specify a number between 1 and 1000."));
                return;
            }
        }
        if (sender instanceof Player player) {
            WorldEditPlugin worldEditPlugin = (WorldEditPlugin) Bukkit.getPluginManager().getPlugin("WorldEdit");
            WorldEdit worldEdit = worldEditPlugin.getWorldEdit();
            if (worldEdit == null) {
                player.sendMessage("I couldn't find the WorldEdit plugin.");
                return;
            }
            com.sk89q.worldedit.entity.Player worldEditPlayer = BukkitAdapter.adapt(player);
            LocalSession undoSession = worldEditPlugin.getSession(player);
            try {
                int timesUndone = 0;
                for (int i = 0; i < numberOfUndoSessions; ++i) {
                    BlockBag blockBag = undoSession.getBlockBag((com.sk89q.worldedit.entity.Player) worldEditPlayer);
                    EditSession undone = undoSession.undo(blockBag, worldEditPlayer);
                    if (undone != null) {
                        timesUndone++;
                        worldEdit.flushBlockBag(worldEditPlayer, undone);
                    } else {
                        break;
                    }
                }
                if (timesUndone == 0) {
                    message.append(
                            "I couldn't undo any world edit sessions.");
                } else {
                    message.append("I undid ")
                            .append(mrGUtils.wrapGreenWhite(Integer.toString(timesUndone)))
                            .append(" world edit session")
                            .append(timesUndone > 1 ? "s." : ".");
                }
                player.sendMessage(message.toString());
            } catch (Exception e) {
                // send the message to the player wrapped in red
                message.append(mrGUtils.wrapRedWhite("I couldn't undo the last world edit session."))
                        .append("(").append(e.getMessage()).append(")");
                player.sendMessage(message.toString());
            }
        }
    }


    @HelpCommand
    public void doHelp(CommandSender sender, CommandHelp help) {
        plugin.bletchLogger.logWarning("doHelp executed.");
        if (sender instanceof Player) {
            Player player = (Player) sender;
            player.sendMessage(ChatColor.GREEN + "Mr G's Plugin Help");
        }
        help.showHelp();
    }
}
