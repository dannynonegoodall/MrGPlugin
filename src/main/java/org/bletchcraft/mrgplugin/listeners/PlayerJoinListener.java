package org.bletchcraft.mrgplugin.listeners;

import org.bletchcraft.mrgplugin.MrGPlugin;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener extends MrGBaseListener implements Listener {
    public PlayerJoinListener(MrGPlugin thePlugin) {
        super(thePlugin);
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        // Logic here to get the protect status for this world

        // Let's re-read the configuration
        plugin.reloadConfig();

    }
}
