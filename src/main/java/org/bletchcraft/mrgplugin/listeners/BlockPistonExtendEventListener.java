package org.bletchcraft.mrgplugin.listeners;

import net.kyori.adventure.text.Component;
import org.bletchcraft.mrgplugin.MrGPlugin;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.BlockCommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.server.ServerCommandEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class BlockPistonExtendEventListener extends MrGBaseListener implements Listener {
    public BlockPistonExtendEventListener(MrGPlugin thePlugin){
        super(thePlugin);
    }

    @EventHandler
    public void onBlockPistonExtendEvent(BlockPistonExtendEvent e){
            // do whatever
        Block block = e.getBlock();
        World world = block.getWorld();

        if (plugin.configManager.worldProtected(world.getName())) {
            plugin.bletchLogger.logWarning("Cancelling a PistonExtendEvent","ServerCommandEvent");
            world.sendMessage(Component.text("MrG has turned off PistonEvents. I think I better remove that block."));
            e.setCancelled(true);
            Bukkit.getScheduler().runTaskLater(plugin, () -> {world.strikeLightning(block.getLocation().add(0,1,0));}, 60);
            Bukkit.getScheduler().runTaskLater(plugin, () -> {block.setType(Material.AIR);}, 80);
        }
    }
}
