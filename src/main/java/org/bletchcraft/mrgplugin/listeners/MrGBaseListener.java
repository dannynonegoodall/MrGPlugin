package org.bletchcraft.mrgplugin.listeners;


import org.bletchcraft.mrgplugin.MrGPlugin;
import org.bletchcraft.mrgplugin.helpers.MrGUtils;

public class MrGBaseListener extends MrGUtils {
    public MrGUtils mrGUtils;
    public MrGBaseListener(final MrGPlugin thePlugin) {
        super(thePlugin);
        mrGUtils = new MrGUtils(thePlugin);
    }
}
