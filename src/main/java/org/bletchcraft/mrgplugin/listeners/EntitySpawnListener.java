package org.bletchcraft.mrgplugin.listeners;

import org.bletchcraft.mrgplugin.MrGPlugin;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

public class EntitySpawnListener extends MrGBaseListener implements Listener {
    public EntitySpawnListener(MrGPlugin thePlugin){
        super(thePlugin);
    }
    @EventHandler
    private void onEntitySpawn(EntitySpawnEvent e){
        Entity entity = e.getEntity();
        String entityType = entity.getType().toString();
        if (plugin.configManager.cannotSpawn(entityType)){
            e.setCancelled(true);
        }
    }
}
